# JavaFx
# JavaFx Employee Catalog Example

### Requirements

* [Java 8](https://www.oracle.com/java/technologies/javase-jdk8-downloads.html)
* [Maven](https://maven.apache.org/download.cgi)

### Run

For run application we can use next command:
    
    `mvn clean spring-boot:run`

    


