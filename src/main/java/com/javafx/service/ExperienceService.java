package com.javafx.service;

import com.javafx.model.Experience;
import com.javafx.repository.ExperienceRepository;
import org.springframework.stereotype.Service;

@Service
public class ExperienceService {

    private final ExperienceRepository experienceRepository;

    public ExperienceService(ExperienceRepository experienceRepository) {
        this.experienceRepository = experienceRepository;
    }

    public void delete(Experience experience) {
        experienceRepository.delete(experience);
    }

}
