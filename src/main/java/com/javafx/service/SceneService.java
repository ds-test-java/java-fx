package com.javafx.service;

import com.javafx.config.ConfigurationControllers;
import com.javafx.controller.BaseController;
import com.javafx.controller.EmployeeFormController;
import com.javafx.controller.EmployeeListController;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class SceneService {

    public final ConfigurationControllers.View<EmployeeListController> EMPLOYEE_LIST_VIEW;

    public final ConfigurationControllers.View<EmployeeFormController> EMPLOYEE_FORM_VIEW;

    private Stage stage;

    public SceneService(
            @Qualifier("employeeListView") ConfigurationControllers.View<EmployeeListController> employeeListView,
            @Qualifier("employeeFormView") ConfigurationControllers.View<EmployeeFormController> employeeFormView) {
        this.EMPLOYEE_LIST_VIEW = employeeListView;
        this.EMPLOYEE_FORM_VIEW = employeeFormView;
    }

    public void initStage(Stage stage) {
        this.stage = stage;
        this.stage.setScene(new Scene(new Pane()));
    }

    public <C extends BaseController> ConfigurationControllers.View<C> updateActiveView(ConfigurationControllers.View<C> view) {
        view.getController().onViewSetActive();
        stage.getScene().setRoot(view.getView());
        stage.sizeToScene();
        return view;
    }

    public void showScene() {
        stage.show();
    }

}
