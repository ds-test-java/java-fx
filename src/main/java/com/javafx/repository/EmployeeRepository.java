package com.javafx.repository;

import com.javafx.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    @Query(value = "SELECT e FROM Employee e " +
                   "left join fetch e.educations " +
                   "left join fetch e.experiences " +
                   "where e.id = :id")
    Optional<Employee> findById(Long id);
}
