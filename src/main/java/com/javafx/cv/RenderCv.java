package com.javafx.cv;

import com.javafx.model.Education;
import com.javafx.model.Employee;
import com.javafx.model.Experience;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.builder.DynamicReports;
import net.sf.dynamicreports.report.builder.component.Components;
import net.sf.dynamicreports.report.builder.component.HorizontalListBuilder;
import net.sf.dynamicreports.report.builder.component.SubreportBuilder;
import net.sf.dynamicreports.report.constant.HorizontalTextAlignment;
import net.sf.dynamicreports.report.constant.PageType;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Collections;
import java.util.Set;

import static net.sf.dynamicreports.report.builder.DynamicReports.cmp;
import static net.sf.dynamicreports.report.builder.DynamicReports.col;
import static net.sf.dynamicreports.report.builder.DynamicReports.report;
import static net.sf.dynamicreports.report.builder.DynamicReports.type;

public class RenderCv implements Runnable {

    private final Employee employee;
    private final boolean appendExperience;
    private final boolean appendEducation;
    private final File path;

    public RenderCv(Employee employee, boolean appendExperience, boolean appendEducation, File path) {
        this.employee = employee;
        this.appendExperience = appendExperience;
        this.appendEducation = appendEducation;
        this.path = path;
    }

    @Override
    public void run() {
        renderCV(appendEducation, appendExperience, path);
    }

    private void renderCV(boolean appendEducation, boolean appendExperience, File path) {
        render(path.getAbsolutePath(), new EmployeeReportModel(employee,
                                                               getExperiencesOrEmptySet(appendExperience),
                                                               getEducationsOrEmptySet(appendEducation)));
    }

    private Set<Education> getEducationsOrEmptySet(boolean appendEducation) {
        return appendEducation ? employee.getEducations() : Collections.emptySet();
    }

    private Set<Experience> getExperiencesOrEmptySet(boolean appendExperience) {
        return appendExperience ? employee.getExperiences() : Collections.emptySet();
    }

    public void render(String filePath, EmployeeReportModel employee) {
        try {
            JasperReportBuilder report = DynamicReports.report();
            report
                    .pageHeader(
                            buildEmployeeData(employee)
                    )
                    .summary(
                            cmp.verticalGap(70),
                            buildExperienceData(employee),
                            cmp.verticalGap(50),
                            buildEducationData(employee)
                    )
                    .setDataSource(
                            new JRBeanCollectionDataSource(
                                    Collections.singletonList(employee)
                            )
                    )
                    .setPageFormat(PageType.A4)
                    .pageFooter(Components.pageXofY())
                    .toPdf(new FileOutputStream(filePath));
        } catch (Throwable t) {
            throw new RuntimeException("error while rendering report", t);
        }
    }

    private SubreportBuilder buildEducationData(EmployeeReportModel employee) {
        return cmp.subreport(report()
                                     .title(cmp.text("Education"))
                                     .columns(
                                             col.column("Name", "name", type.stringType()),
                                             col.column("Date", "date", type.dateType())
                                                     .setHorizontalTextAlignment(HorizontalTextAlignment.LEFT),
                                             col.column("Certificate Id", "certificateId",
                                                        type.stringType())
                                     ))
                .setDataSource(new JRBeanCollectionDataSource(employee.getEducations()));
    }

    private SubreportBuilder buildExperienceData(EmployeeReportModel employee) {
        return cmp.subreport(report()
                                     .title(cmp.text("Experience"))
                                     .columns(
                                             col.column("Position", "position", type.stringType()),
                                             col.column("From", "dateFrom", type.dateType())
                                                     .setHorizontalTextAlignment(HorizontalTextAlignment.LEFT),
                                             col.column("To", "dateTo", type.dateType())
                                                     .setHorizontalTextAlignment(HorizontalTextAlignment.LEFT)
                                     )
                                     .setDataSource(new JRBeanCollectionDataSource(employee.getExperiences())));
    }

    private HorizontalListBuilder buildEmployeeData(EmployeeReportModel employee) {
        return cmp.horizontalList(
                cmp.verticalList(
                        cmp.text("First Name"),
                        cmp.text("Last Name"),
                        cmp.text("Department"),
                        cmp.text("Position")
                ),
                cmp.verticalList(
                        cmp.text(employee.getFirstName()),
                        cmp.text(employee.getLastName()),
                        cmp.text(employee.getDepartmentType().toString()),
                        cmp.text(employee.getPositionType().toString())
                )
        );
    }
}
