package com.javafx.cv;

import com.javafx.model.DepartmentType;
import com.javafx.model.Education;
import com.javafx.model.Employee;
import com.javafx.model.Experience;
import com.javafx.model.PositionType;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
public class EmployeeReportModel {

    private Long id;

    private String firstName;

    private String lastName;

    private DepartmentType departmentType;

    private PositionType positionType;

    private List<Experience> experiences;

    private List<Education> educations;

    public EmployeeReportModel(Employee employee,
                               Set<Experience> experiences,
                               Set<Education> educations) {
        this.id = employee.getId();
        this.firstName = employee.getFirstName();
        this.lastName = employee.getLastName();
        this.departmentType = employee.getDepartmentType();
        this.positionType = employee.getPositionType();
        this.experiences = new ArrayList<>(experiences);
        this.educations = new ArrayList<>(educations);
    }
}
