package com.javafx.model;

public enum PositionType {
    DEVELOPER, DEVOPS, TESTER, TEAMLEAD
}
