package com.javafx.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.sql.Date;

@Data
@Entity
@Table(name = "Experience")
public class Experience {

    @Id
    @GeneratedValue
    private Long id;

    @NotBlank
    private String position;

    @NotNull
    private Date dateFrom;

    @NotNull
    private Date dateTo;

    @ManyToOne
    @JoinColumn(name = "employee")
    private Employee employee;

}
