package com.javafx.model;

public enum DepartmentType {
    JAVA, C, CPP, PYTHON, GO
}
