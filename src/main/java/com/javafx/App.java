package com.javafx;

import com.javafx.service.SceneService;
import javafx.application.Application;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class App extends Application {

    @Autowired
    private SceneService sceneService;

    private ConfigurableApplicationContext context;

    @Override
    public void init() {
        context = SpringApplication.run(getClass());
        context.getAutowireCapableBeanFactory().autowireBean(this);
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        context.close();
    }

    @Override
    public void start(Stage stage) {
        sceneService.initStage(stage);
        sceneService.updateActiveView(sceneService.EMPLOYEE_LIST_VIEW);
        sceneService.showScene();
    }

    public static void main(String[] args) {
        launch();
    }

}