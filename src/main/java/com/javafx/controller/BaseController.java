package com.javafx.controller;

public interface BaseController {

    void onViewSetActive();
}
