package com.javafx.controller;

import com.javafx.config.ConfigurationControllers;
import com.javafx.model.DepartmentType;
import com.javafx.model.Employee;
import com.javafx.model.PositionType;
import com.javafx.service.EmployeeService;
import com.javafx.service.SceneService;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import org.springframework.beans.factory.annotation.Autowired;

public class EmployeeListController implements BaseController {

    private static final String EDIT_IMAGE_PATH = "/images/edit.png";
    private static final String DELETE_IMAGE_PATH = "/images/delete.png";

    private static final String removeAlertMessage = "Are you sure you want to remove the employee from the database?";

    public TableView<Employee> employersTable;
    public TableColumn<Employee, String> columnName;
    public TableColumn<Employee, DepartmentType> columnDepartment;
    public TableColumn<Employee, PositionType> columnPosition;
    public TableColumn<Employee, Object> columnAction;

    public Button addEmployeeButton;

    public ObservableList<Employee> employers;

    @Autowired
    private SceneService sceneService;

    @Autowired
    private EmployeeService employeeService;

    @Override
    public void onViewSetActive() {
        employers.clear();
        employers.addAll(employeeService.findAll());
    }

    public void initialize() {
        employers = FXCollections.observableArrayList();
        employersTable.setItems(employers);

        initColumns();
        initActionButtons();
    }

    private void initActionButtons() {
        addEmployeeButton.setOnAction(event -> sceneService.updateActiveView(sceneService.EMPLOYEE_FORM_VIEW));
    }

    private void initColumns() {
        columnName.setCellValueFactory(param -> new SimpleStringProperty(getEmployeeFullName(param)));
        columnDepartment.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getDepartmentType()));
        columnPosition.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getPositionType()));
        columnAction.setCellFactory(param -> new ActionButtonsTableCell());
    }

    private String getEmployeeFullName(TableColumn.CellDataFeatures<Employee, String> param) {
        return param.getValue().getFirstName() + " " + param.getValue().getLastName();
    }

    private class ActionButtonsTableCell extends TableCell<Employee, Object> {
        @Override
        protected void updateItem(Object o, boolean isEmpty) {
            super.updateItem(o, isEmpty);

            if (returnIfEmptyRow(isEmpty)) {
                return;
            }

            final Button editEmployeeButton = createEditButton();
            final Button delete = createDeleteButton();
            final HBox hBox = createHBoxWithButtons(editEmployeeButton, delete);

            setGraphic(hBox);
        }

        private boolean returnIfEmptyRow(boolean isEmpty) {
            if (isEmpty) {
                setGraphic(null);
                return true;
            }

            return false;
        }

        private Button createDeleteButton() {
            final Button deleteEmployeeButton = buildActionButton(DELETE_IMAGE_PATH);

            deleteEmployeeButton.setOnAction(event -> {
                Alert alert = createEmployeeDeleteAlert();
                alert.showAndWait();

                if (alert.getResult() == ButtonType.OK) {
                    employeeService.delete(employers.get(getIndex()));
                    employers.remove(getIndex());
                }
            });

            return deleteEmployeeButton;
        }

        private Button createEditButton() {
            final Button editButton = buildActionButton(EDIT_IMAGE_PATH);

            editButton.setOnAction(event -> {
                final ConfigurationControllers.View<EmployeeFormController> view =
                        sceneService.updateActiveView(sceneService.EMPLOYEE_FORM_VIEW);
                view.getController().fillForm(employeeService.findById(employers.get(getIndex()).getId()).get());
            });

            return editButton;
        }

        private Button buildActionButton(String imagePath) {
            final Button button = new Button();
            final ImageView buttonImage = new ImageView(
                    new Image(imagePath, 15, 15, false, false));

            button.setGraphic(buttonImage);

            return button;
        }

        private HBox createHBoxWithButtons(Button editEmployeeButton, Button deleteEmployeeButton) {
            final HBox hBox = new HBox(editEmployeeButton, deleteEmployeeButton);

            hBox.setSpacing(5.0);
            hBox.setAlignment(Pos.CENTER);

            return hBox;
        }

        private Alert createEmployeeDeleteAlert() {
            final Alert alert = new Alert(Alert.AlertType.CONFIRMATION,
                                          removeAlertMessage,
                                          ButtonType.CANCEL,
                                          ButtonType.OK);

            alert.setTitle("Delete Employee?");

            return alert;
        }
    }
}
