package com.javafx.controller.element;

import javafx.scene.control.Button;
import javafx.scene.control.TableCell;

import java.util.function.Consumer;

public class ControlColumnTableCell<T> extends TableCell<T, Object> {

    private final Consumer<Integer> onDeleteAction;

    public ControlColumnTableCell(Consumer<Integer> onDeleteAction) {
        this.onDeleteAction = onDeleteAction;
    }

    @Override
    protected void updateItem(Object o, boolean isEmpty) {
        super.updateItem(o, isEmpty);

        if (isEmpty) {
            setGraphic(null);
            return;
        }

        final Button removeRowButton = new Button("-");

        removeRowButton.setOnAction(event -> removeTableRow());
        setGraphic(removeRowButton);
    }

    private void removeTableRow() {
        onDeleteAction.accept(getIndex());
        getTableView().getItems().remove(getIndex());
    }
}