package com.javafx.controller.element;

import com.javafx.model.Education;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableCell;

public class EducationCertificateColumnTableCell extends TableCell<Education, Boolean> {

    @Override
    protected void updateItem(Boolean item, boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
            setGraphic(null);
            return;
        }

        final Education education = getTableView().getItems().get(getIndex());
        final CheckBox checkBox = new CheckBox();

        checkBox.setOnAction(event -> updateIsCertificated(education, checkBox));
        checkBox.setSelected(education.getCertificateId() != null);

        setGraphic(checkBox);
    }

    private void updateIsCertificated(Education education, CheckBox checkBox) {
        education.getCertificated().set(checkBox.isSelected());
    }
}
