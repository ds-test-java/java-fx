package com.javafx.controller.element;

import javafx.scene.control.TableCell;
import javafx.scene.control.TextField;

import java.util.function.BiConsumer;
import java.util.function.Function;

public class TextColumnTableCell<T> extends TableCell<T, String> {

    private final Function<T, String> onInitValue;

    private final BiConsumer<T, String> onUpdateValue;

    public TextColumnTableCell(Function<T, String> onInitValue,
                               BiConsumer<T, String> onUpdateValue) {
        this.onInitValue = onInitValue;
        this.onUpdateValue = onUpdateValue;
    }

    @Override
    public void updateItem(String s, boolean isEmpty) {
        super.updateItem(s, isEmpty);

        if (isEmpty) {
            setText(null);
            setGraphic(null);
            return;
        }

        final TextField textField = new TextField();
        final String initValue = onInitValue.apply(getElement());

        textField.setText(initValue);
        textField.setPromptText("required");
        textField.textProperty().addListener((observable, oldValue, newValue) -> updateName(newValue));

        setGraphic(textField);
    }

    private void updateName(String newValue) {
        onUpdateValue.accept(getElement(), newValue);
        commitEdit(newValue);
    }

    private T getElement() {
        return getTableView().getItems().get(getIndex());
    }
}