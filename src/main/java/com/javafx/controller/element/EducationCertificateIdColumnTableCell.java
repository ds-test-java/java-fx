package com.javafx.controller.element;

import com.javafx.model.Education;
import javafx.scene.control.TableCell;
import javafx.scene.control.TextField;

public class  EducationCertificateIdColumnTableCell extends TableCell<Education, String> {

    @Override
    protected void updateItem(String s, boolean isEmpty) {
        super.updateItem(s, isEmpty);

        if (isEmpty) {
            setGraphic(null);
            return;
        }

        final Education education = getTableView().getItems().get(getIndex());
        final TextField textField = new TextField();

        textField.editableProperty().bind(education.getCertificated());
        education.getCertificated().set(education.getCertificateId() != null);
        textField.setText(getTableView().getItems().get(getIndex()).getCertificateId());

        textField.textProperty().addListener((observable, oldValue, newValue) ->
                                                     updateCertificateId(education, newValue));

        setGraphic(textField);
    }

    private void updateCertificateId(Education education, String newValue) {
        education.setCertificateId(newValue);
        commitEdit(newValue);
    }
}
