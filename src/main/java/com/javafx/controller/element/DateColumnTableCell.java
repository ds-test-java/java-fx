package com.javafx.controller.element;

import javafx.event.Event;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;

import java.sql.Date;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.function.BiFunction;
import java.util.function.Function;

public class DateColumnTableCell<T> extends TableCell<T, Date> {
    private final BiFunction<Integer, Date, Boolean> filter;
    private DatePicker datePicker;
    private Function<Integer, Date> modelDate;

    public DateColumnTableCell(Function<Integer, Date> modelDate, BiFunction<Integer, Date, Boolean> filter) {
        this.modelDate = modelDate;
        this.filter = filter;
    }

    public DateColumnTableCell(Function<Integer, Date> modelDate) {
        this.modelDate = modelDate;
        this.filter = (index, date) -> true;
    }

    @Override
    public void startEdit() {
        if (!isEmpty()) {
            super.startEdit();

            setText(null);
            setGraphic(datePicker);
        }
    }

    @Override
    public void updateItem(Date item, boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
            setText(null);
            setGraphic(null);
        } else {
            createDatePicker();
            datePicker.setValue(getDate().toLocalDate());
            setText(null);
            setGraphic(datePicker);
        }
    }

    private void createDatePicker() {
        datePicker = new DatePicker(getDate().toLocalDate());
        datePicker.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
        datePicker.setDayCellFactory(picker -> createDateCell());
        datePicker.setOnAction(event -> fireCustomEditEvent(convertDate(datePicker.getValue())));
    }

    private DateCell createDateCell() {
        return new DateCell() {
            public void updateItem(LocalDate date, boolean empty) {
                super.updateItem(date, empty);
                setDisable(empty || !filter.apply(getIndex(), convertDate(date)));
            }
        };
    }

    private Date convertDate(LocalDate date) {
        return new Date(date.atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli());
    }

    private void fireCustomEditEvent(Date value) {
        final TableColumn.CellEditEvent<T, Date> editEvent =
                new TableColumn.CellEditEvent<>(getTableView(),
                                                new TablePosition<>(getTableView(), getIndex(), getTableColumn()),
                                                TableColumn.editCommitEvent(),
                                                value
                );
        Event.fireEvent(getTableColumn(), editEvent);
    }

    private Date getDate() {
        final Date date = modelDate.apply(getIndex());

        return date == null ? new Date(Instant.now().toEpochMilli()) : date;
    }
}