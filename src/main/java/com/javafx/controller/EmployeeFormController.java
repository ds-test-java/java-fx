package com.javafx.controller;

import com.javafx.controller.element.ControlColumnTableCell;
import com.javafx.controller.element.DateColumnTableCell;
import com.javafx.controller.element.EducationCertificateColumnTableCell;
import com.javafx.controller.element.EducationCertificateIdColumnTableCell;
import com.javafx.controller.element.TextColumnTableCell;
import com.javafx.cv.RenderCv;
import com.javafx.model.DepartmentType;
import com.javafx.model.Education;
import com.javafx.model.Employee;
import com.javafx.model.Experience;
import com.javafx.model.PositionType;
import com.javafx.service.EducationService;
import com.javafx.service.EmployeeService;
import com.javafx.service.ExperienceService;
import com.javafx.service.SceneService;
import com.javafx.util.FileChooserUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import java.io.File;
import java.sql.Date;
import java.time.Instant;
import java.util.Collections;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Slf4j
public class EmployeeFormController implements BaseController {

    private static final String CV_DEFAULT_FILE_NAME = "CV.pdf";

    public Employee formEmployee;

    public TextField firstName;
    public TextField lastName;
    public ChoiceBox<DepartmentType> departmentBox;
    public ChoiceBox<PositionType> positionBox;

    public TableView<Education> educationTable;
    public TableColumn<Education, Object> educationControlColumn;
    public TableColumn<Education, String> educationNameColumn;
    public TableColumn<Education, Date> educationDateColumn;
    public TableColumn<Education, Boolean> educationCertificateColumn;
    public TableColumn<Education, String> educationCertificateIdColumn;

    public TableView<Experience> experienceTable;
    public TableColumn<Experience, Object> experienceControlColumn;
    public TableColumn<Experience, String> experiencePositionColumn;
    public TableColumn<Experience, Date> experienceDateFromColumn;
    public TableColumn<Experience, Date> experienceDateToColumn;

    public Button cancelButton;
    public Button saveButton;
    public Button cvButton;

    private ObservableList<Education> educationObservable;
    private ObservableList<Experience> experienceObservable;

    private static final String PREPARE_CV_BUTTON_LABEL = "Prepare CV";
    private static final String CANCEL_BUTTON_LABEL = "Cancel";
    private static final String INCLUDE_EDUCATION_TEXT = "Include \"Education\"";
    private static final String INCLUDE_WORK_EXP_TEXT = "Include \"Work Experience\"";

    @Autowired
    private SceneService sceneService;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private EducationService educationService;

    @Autowired
    private ExperienceService experienceService;

    @Override
    public void onViewSetActive() {
        educationObservable.clear();
        experienceObservable.clear();

        fillForm(null);
    }

    public void initialize() {
        educationObservable = FXCollections.observableArrayList();
        experienceObservable = FXCollections.observableArrayList();

        initChoiceBoxes();
        initActionButtons();
        initEmployeeFormListeners();
        initEducationTable();
        initExperienceTable();
    }

    public void fillForm(Employee employee) {
        formEmployee = getOrInitializeEmployee(employee);

        firstName.setText(formEmployee.getFirstName());
        lastName.setText(formEmployee.getLastName());
        departmentBox.setValue(formEmployee.getDepartmentType());
        positionBox.setValue(formEmployee.getPositionType());
        educationObservable.addAll(formEmployee.getEducations());
        experienceObservable.addAll(formEmployee.getExperiences());
    }

    private Employee getOrInitializeEmployee(Employee employee) {
        return (employee == null) ? new Employee() : employee;
    }

    private void initChoiceBoxes() {
        departmentBox.getItems().addAll(DepartmentType.values());
        positionBox.getItems().addAll(PositionType.values());
    }

    private void initActionButtons() {
        saveButton.setOnAction(event -> saveButtonAction());

        cancelButton.setOnAction(event -> sceneService.updateActiveView(sceneService.EMPLOYEE_LIST_VIEW));

        cvButton.setOnAction(event -> showCvSaveDialog());
    }

    private void saveButtonAction() {
        formEmployee.setEducations(getFilledEducations());
        formEmployee.setExperiences(getFilledExperiences());
        try {
            employeeService.save(formEmployee);
        } catch (TransactionSystemException e) {
            log.error(e.getMessage(), e);
            new Alert(Alert.AlertType.ERROR, e.getLocalizedMessage()).showAndWait();
        }
    }

    private void initEmployeeFormListeners() {
        firstName.textProperty().addListener((observable, oldValue, newValue) -> formEmployee.setFirstName(newValue));
        lastName.textProperty().addListener((observable, oldValue, newValue) -> formEmployee.setLastName(newValue));
        departmentBox.setOnAction(actionEvent -> formEmployee.setDepartmentType(departmentBox.getValue()));
        positionBox.setOnAction(actionEvent -> formEmployee.setPositionType(positionBox.getValue()));
    }

    private Set<Education> getFilledEducations() {
        return educationObservable.stream()
                .filter(education -> education.getName() != null)
                .collect(Collectors.toSet());
    }

    private Set<Experience> getFilledExperiences() {
        return experienceObservable.stream()
                .filter(experience -> experience.getPosition() != null)
                .collect(Collectors.toSet());
    }

    private void showCvSaveDialog() {
        final ButtonType cvDialogButton = new ButtonType(PREPARE_CV_BUTTON_LABEL, ButtonBar.ButtonData.APPLY);
        final ButtonType cancelButton = new ButtonType(CANCEL_BUTTON_LABEL, ButtonBar.ButtonData.CANCEL_CLOSE);

        final Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "");
        final CheckBox includeEducation = new CheckBox(INCLUDE_EDUCATION_TEXT);
        final CheckBox includeExperience = new CheckBox(INCLUDE_WORK_EXP_TEXT);
        final VBox box = new VBox(includeEducation, includeExperience);

        alert.getButtonTypes().clear();
        alert.getButtonTypes().addAll(cancelButton, cvDialogButton);
        box.setSpacing(20);
        alert.setGraphic(box);

        showSaveCvDialog(cvDialogButton, alert, includeEducation, includeExperience);
    }

    private void showSaveCvDialog(ButtonType okButton,
                                  Alert alert,
                                  CheckBox includeEducationCb,
                                  CheckBox includeExperienceCb) {
        alert.showAndWait().ifPresent(type -> {
            if (type == okButton) {
                final boolean appendEducation = includeEducationCb.isSelected();
                final boolean appendExperience = includeExperienceCb.isSelected();

                final File savePath = FileChooserUtil.chooseFilePath(cvButton.getScene().getWindow(),
                        CV_DEFAULT_FILE_NAME,
                        "Document",
                        Collections.singletonList("*.pdf"));

                if (savePath != null) {
                    new Thread(new RenderCv(formEmployee, appendExperience, appendEducation, savePath)).start();
                }
            }
        });
    }

    private void initEducationTable() {
        educationTable.setItems(educationObservable);
        educationTable.setEditable(true);

        final Button addNewRowButton = new Button("+");
        addNewRowButton.setOnMouseClicked(event -> createEducationTableRow());

        educationControlColumn.setSortable(false);
        educationControlColumn.setGraphic(addNewRowButton);
        educationControlColumn.setCellFactory(model -> new ControlColumnTableCell<>(getEducationDeleteRowAction()));

        educationNameColumn.setCellFactory(model -> createEducationNameTableCell());
        educationNameColumn.setOnEditCommit(this::setEducationName);

        educationDateColumn.setCellFactory(param -> createEducationDateColumnTableCell());
        educationDateColumn.setOnEditCommit(this::setEducationDate);

        educationCertificateColumn.setCellFactory(param -> new EducationCertificateColumnTableCell());

        educationCertificateIdColumn.setCellFactory(model -> new EducationCertificateIdColumnTableCell());
    }

    private TextColumnTableCell<Education> createEducationNameTableCell() {
        return new TextColumnTableCell<>(Education::getName, Education::setName);
    }

    private DateColumnTableCell<Education> createEducationDateColumnTableCell() {
        return new DateColumnTableCell<>(index -> educationObservable.get(index).getDate());
    }

    private void createEducationTableRow() {
        final Education education = new Education();

        education.setDate(new Date(Instant.now().toEpochMilli()));
        education.setEmployee(formEmployee);
        educationObservable.add(education);
    }

    private void setEducationName(TableColumn.CellEditEvent<Education, String> event) {
        Education education = getTableItem(event.getTableView(), event.getTablePosition().getRow());
        education.setName(event.getNewValue());
    }

    private void setEducationDate(TableColumn.CellEditEvent<Education, Date> event) {
        Education education = getTableItem(event.getTableView(), event.getTablePosition().getRow());
        education.setDate(event.getNewValue());
    }

    private void initExperienceTable() {
        experienceTable.setItems(experienceObservable);
        experienceTable.setEditable(true);

        final Button addNewRowButton = new Button("+");
        addNewRowButton.setOnMouseClicked(event -> createExperienceTableRow());

        experienceControlColumn.setSortable(false);
        experienceControlColumn.setGraphic(addNewRowButton);
        experienceControlColumn.setCellFactory(model -> new ControlColumnTableCell<>(getExperienceDeleteRowAction()));

        experiencePositionColumn.setCellFactory(model -> createExperiencePositionTableCell());

        experienceDateFromColumn.setCellFactory(param -> createExperienceDateFromColumnTableCell());
        experienceDateFromColumn.setOnEditCommit(this::setExperienceDataFrom);

        experienceDateToColumn.setCellFactory((param -> createExperienceDateToColumnTableCell()));
        experienceDateToColumn.setOnEditCommit(this::setExperienceDateTo);
    }

    private TextColumnTableCell<Experience> createExperiencePositionTableCell() {
        return new TextColumnTableCell<>(Experience::getPosition, Experience::setPosition);
    }

    private DateColumnTableCell<Experience> createExperienceDateFromColumnTableCell() {
        return new DateColumnTableCell<>(
                index -> experienceObservable.get(index).getDateFrom(),
                getDateFromActiveDatesFilter()
        );
    }

    private DateColumnTableCell<Experience> createExperienceDateToColumnTableCell() {
        return new DateColumnTableCell<>(
                index -> experienceObservable.get(index).getDateTo(),
                getDateToActiveDatesFilter()
        );
    }

    private void setExperienceDateTo(TableColumn.CellEditEvent<Experience, Date> event) {
        final Experience experience = getTableItem(event.getTableView(), event.getTablePosition().getRow());
        experience.setDateTo(event.getNewValue());
    }

    private void setExperienceDataFrom(TableColumn.CellEditEvent<Experience, Date> event) {
        final Experience experience = getTableItem(event.getTableView(), event.getTablePosition().getRow());
        experience.setDateFrom(event.getNewValue());
    }

    private <T> T getTableItem(TableView<T> tableView, int position) {
        return tableView.getItems().get(position);
    }

    private void createExperienceTableRow() {
        final Experience experience = new Experience();
        final Date defaultDate = new Date(Instant.now().toEpochMilli());

        experience.setDateFrom(defaultDate);
        experience.setDateTo(defaultDate);
        experience.setEmployee(formEmployee);

        experienceObservable.add(experience);
    }

    private Consumer<Integer> getEducationDeleteRowAction() {
        return index -> educationService.delete(educationObservable.get(index));
    }

    private Consumer<Integer> getExperienceDeleteRowAction() {
        return index -> experienceService.delete(experienceObservable.get(index));
    }

    private BiFunction<Integer, Date, Boolean> getDateFromActiveDatesFilter() {
        return (index, date) -> date.before(experienceObservable.get(index).getDateTo());
    }

    private BiFunction<Integer, Date, Boolean> getDateToActiveDatesFilter() {
        return (index, date) -> date.after(experienceObservable.get(index).getDateFrom());
    }

}
