package com.javafx.config;

import com.javafx.controller.BaseController;
import com.javafx.controller.EmployeeFormController;
import com.javafx.controller.EmployeeListController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.net.URL;

@Configuration
public class ConfigurationControllers {

    @Bean(name = "employeeListView")
    public View<EmployeeListController> getEmployeeListView() throws IOException {
        return loadView("/fxml/employee-list.fxml");
    }

    @Bean(name = "employeeFormView")
    public View<EmployeeFormController> getEmployeeFormView() throws IOException {
        return loadView("/fxml/employee-form.fxml");
    }

    @Bean
    public EmployeeListController getEmployeeListController() throws IOException {
        return getEmployeeListView().getController();
    }

    @Bean
    public EmployeeFormController getEmployeeFormController() throws IOException {
        return getEmployeeFormView().getController();
    }

    protected <C extends BaseController> View<C> loadView(String url) throws IOException {
        URL resourceUrl = getClass().getResource(url);
        final FXMLLoader loader = new FXMLLoader(resourceUrl);
        return new View<>(loader.load(), loader.getController());
    }

    public static class View<C extends BaseController> {
        private Parent view;
        private C controller;

        public View(Parent view, C controller) {
            this.view = view;
            this.controller = controller;
        }

        public Parent getView() {
            return view;
        }

        public void setView(Parent view) {
            this.view = view;
        }

        public C getController() {
            return controller;
        }

        public void setController(C controller) {
            this.controller = controller;
        }
    }
}
